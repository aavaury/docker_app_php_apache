FROM php:8.2-apache
RUN docker-php-ext-install mysqli

# Activez le module mod_rewrite
RUN a2enmod rewrite
RUN sed -i 's/AllowOverride None/AllowOverride All/' /etc/apache2/apache2.conf

# Supprime le site par défaut d'Apache
RUN rm -rf /var/www/html/*

# Copie le contenu de votre application
COPY ./www /var/www/html