# Docker_app_php_apache

Ce projet utilise Docker pour créer un environnement de développement avec un site web, une base de données, et une interface PhpMyAdmin.

<img src="https://www.bgs-associes.com/wp-content/uploads/2021/03/homepage-docker-logo.png" width="200"/>

## Prérequis

- Docker
- Docker Compose


L'architecture de votre app doit ressembler a cell-ci

 ```bash
.
├── docker-compose.yml
├── Dockerfile
├── databse
│   └── my_bdd.sql
└── www
    └── index.php

 ```
   
## Comment utiliser

1. Clone le repository:

    ```bash
    git clone https://gitlab.com/aavaury/docker_app_php_apache.git
    ```

2. Accède au répertoire :

    ```bash
    cd Docker_app_php_apache
    ```

3. Lance les conteneurs:

    ```bash
    docker-compose up -d
    ```

   Le site web sera accessible à l'adresse [http://localhost](http://localhost), et PhpMyAdmin à [http://localhost:8000](http://localhost:8000).

4. Pour arrêter les conteneurs:

    ```bash
    docker-compose down
    ```

## Configuration

- Le site web est accessible à [http://localhost](http://localhost).
- PhpMyAdmin est accessible à [http://localhost:8000](http://localhost:8000).
- Les informations de connexion pour la base de données sont :

  - Host: `localhost`
  - Username: `user`
  - Password: `test`
  - Database: `my_bdd`

## Personnalisation

- Personnalise les ports dans le fichier `docker-compose.yml`.
- Les fichiers du site web sont placés dans le répertoire `www`.
